(function() {
	'use strict';

	angular
		.module('app.services')
		.factory('Manufacturer', Manufacturer);

	Manufacturer.$inject = ['restmod'];

	/* @ngInject */
	function Manufacturer(restmod) {
        return restmod.model('http://ittc-dev.astate.edu/abhishek/inventory-backend/public/api/member/manufacturers').mix({
			products: { hasMany: 'Product' }
		});
	}
})();
