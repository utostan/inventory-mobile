(function() {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('ProductDetailController', ProductDetailController);

    ProductDetailController.$inject = ['Product', '$stateParams', '$ionicLoading', 'Reservation', '$ionicPopup', '$state', '$ionicHistory'];

    /* @ngInject */
    function ProductDetailController(Product, $stateParams, $ionicLoading, Reservation, $ionicPopup, $state, $ionicHistory) {
        var vm = this;
        vm.products = [];
        vm.getProduct = getProduct;
        vm.getItems = getItems;
        vm.getManufacturer = getManufacturer;
        vm.reserveDisabled = true;
        vm.reserve = reserve;
        vm.imagePath = 'http://ittc-dev.astate.edu/abhishek/inventory-backend/public/images/';

        activate();

        function activate() {
            $ionicLoading.show({ template: '<ion-spinner icon="ripple"></ion-spinner>' });
            var product = getProduct();
            getItems(product);
        }

        function getProduct()
        {

            var product = Product.$find($stateParams.productId);
            product.$then(function(data) {
                vm.product = data.$response.data.product;
                getManufacturer(data);
            });

            return product;
        }

        function getItems(product)
        {
            var items = product.items.$fetch();
            items.$then(function(data) {
                vm.items = data.$response.data.items;
                if (vm.items.length > 0)
                    vm.reserveDisabled = false;
            });
        }

        function getManufacturer(product)
        {
            var manufacturer = product.manufacturer.$fetch();
            manufacturer.$then(function(data) {
                vm.manufacturer = data.$response.data.manufacturer;
                $ionicLoading.hide();
            });
        }

        function reserve(id)
        {
            var reservation = Reservation.$create({ product_id: id });
            reservation.$then(function(success) {
                var alertPopup = $ionicPopup.alert({
                  title: 'Item Reserved',
                  template: 'Your item has been reserved'
                });

                alertPopup.then(function(res) {
                    $ionicHistory.nextViewOptions({
                        disableBack: true
                      });
                    $state.go('app.welcome', {}, { reload: true });
                });
            }, function(fail) {
                   var alertPopup = $ionicPopup.alert({
                     title: 'Reservations Exceeded',
                     template: 'You cannot reserve more than two items at a time'
                   });

                   alertPopup.then(function(res) {
                     
                   });
            });
        }
    }
})();
