(function() {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('CategoryDetailController', CategoryDetailController);

    CategoryDetailController.$inject = ['Category', '$stateParams', '$ionicLoading'];

    /* @ngInject */
    function CategoryDetailController(Category, $stateParams, $ionicLoading) {
        var vm = this;
        vm.products = [];
        vm.getCategory = getCategory;
        vm.getProducts = getProducts;
        vm.imagePath = 'http://ittc-dev.astate.edu/abhishek/inventory-backend/public/images/';

        activate();

        function activate() {
            $ionicLoading.show({ template: '<ion-spinner icon="ripple"></ion-spinner>' });
            var category = getCategory();
            getProducts(category);
        }

        function getCategory()
        {
            var category = Category.$find($stateParams.categoryId);
            category.$then(function(data) {
                vm.category = data.$response.data.category;
            });

            return category;
        }

        function getProducts(category)
        {
            var products = category.products.$fetch();
            products.$then(function(data) {
                vm.products = products.$response.data.products;
                $ionicLoading.hide();
            });
        }
    }
})();
