(function () {
    'use strict';

    angular
        .module('app.auth')
        .config(config);

    config.$inject = ['$stateProvider'];

    function config($stateProvider) {
        $stateProvider
            .state('app.login', {
                url: '/login',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/login.html',
                        controller: 'AuthController',
                        controllerAs: 'auth'
                    }
                }
            })
            .state('app.register', {
                url: '/register',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/register.html',
                        controller: 'AuthController',
                        controllerAs: 'auth'
                    }
                }
            });
    }
})();
