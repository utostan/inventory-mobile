(function() {
	'use strict';

	angular
		.module('app.services')
		.factory('Product', Product);

	Product.$inject = ['restmod'];

	/* @ngInject */
	function Product(restmod) {
        return restmod.model('http://ittc-dev.astate.edu/abhishek/inventory-backend/public/api/member/products').mix({
            category: { belongsTo: 'Category' },
            manufacturer: { belongsTo: 'Manufacturer' },
            items: { hasMany: 'Item' }
        });
	}
})();
