(function() {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('ProductController', ProductController);

    ProductController.$inject = ['Product', '$ionicLoading'];

    /* @ngInject */
    function ProductController(Product, $ionicLoading) {
        var vm = this;
        vm.getProducts = getProducts;
        vm.products = [];
        vm.imagePath = 'http://ittc-dev.astate.edu/abhishek/inventory-backend/public/images/';

        activate();

        function activate() {
            $ionicLoading.show({ template: '<ion-spinner icon="ripple"></ion-spinner>' });
            getProducts();
        }

        function getProducts() {
            var products = Product.$search();
            products.$then(function(data) {
                vm.products = data.$response.data.products;
                $ionicLoading.hide();
            });
        }
    }
})();
