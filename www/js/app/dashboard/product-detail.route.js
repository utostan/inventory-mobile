(function () {
    'use strict';

    angular
        .module('app.dashboard')
        .config(config);

    config.$inject = ['$stateProvider'];

    function config($stateProvider) {
        $stateProvider
            .state('app.product', {
                url: '/product/:productId',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/product.html',
                        controller: 'ProductDetailController',
                        controllerAs: 'product'
                    }
                }
            });
    }
})();
