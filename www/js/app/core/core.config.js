(function () {
    'use strict';

    angular
        .module('app.core')
        .config(config);

    config.$inject = ['$urlRouterProvider', '$stateProvider', '$ionicConfigProvider'];

    function config($urlRouterProvider, $stateProvider, $ionicConfigProvider) {
        $stateProvider
            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'templates/menu.html',
                controller: 'CoreController',
                controllerAs: 'core'
            });

        $urlRouterProvider.otherwise('/app/login');

        $ionicConfigProvider.views.maxCache(0);
    }
})();
