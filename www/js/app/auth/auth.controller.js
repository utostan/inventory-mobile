(function() {
    'use strict';

    angular
        .module('app.auth')
        .controller('AuthController', AuthController);

    AuthController.$inject = ['$auth', '$state', '$ionicHistory'];

    /* @ngInject */
    function AuthController($auth, $state, $ionicHistory) {
        var vm = this;
        vm.isAuthenticated = isAuthenticated;
        vm.login = login;
        vm.register = register

        activate();

        function activate() {
            if (isAuthenticated()) {
                $ionicHistory.nextViewOptions({
                    disableBack: true
                  });
                $state.go('app.welcome', {});
            }
        }

        function isAuthenticated() {
			return $auth.isAuthenticated();
		}

        function login() {
            var credentials = {
				email: vm.email,
				password: vm.password
			};

			$auth.login(credentials).then(function(data) {
                $ionicHistory.nextViewOptions({
                    disableBack: true
                  });
				$state.go('app.welcome', {});
			}).catch(function(response) {
				vm.error = response.data.error;
			});
        }

        function register() {
            var user = {
                first_name: vm.first_name,
                last_name: vm.last_name,
                email: vm.email,
                password: vm.password
            }

            $auth.signup(user).then(function(response) {
                $ionicHistory.nextViewOptions({
                    disableBack: true
                  });
                $state.go('app.login', {});
            }).catch(function(response) {
                vm.error = response.data.error;
            });
        }
    }
})();
