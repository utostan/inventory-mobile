(function() {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('CategoryController', CategoryController);

    CategoryController.$inject = ['Category', '$ionicLoading'];

    /* @ngInject */
    function CategoryController(Category, $ionicLoading) {
        var vm = this;
        vm.getCategories = getCategories;
        vm.categories = [];

        activate();

        function activate() {
            $ionicLoading.show({ template: '<ion-spinner icon="ripple"></ion-spinner>' });
            getCategories();
        }

        function getCategories() {
            var categories = Category.$search();
            categories.$then(function(data) {
                vm.categories = data.$response.data.categories;
                $ionicLoading.hide();
            });
        }
    }
})();
