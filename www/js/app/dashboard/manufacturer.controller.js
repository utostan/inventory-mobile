(function() {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('ManufacturerController', ManufacturerController);

    ManufacturerController.$inject = ['Manufacturer', '$ionicLoading'];

    /* @ngInject */
    function ManufacturerController(Manufacturer, $ionicLoading) {
        var vm = this;
        vm.getManufacturers = getManufacturers;
        vm.manufacturers = [];

        activate();

        function activate() {
            $ionicLoading.show({ template: '<ion-spinner icon="ripple"></ion-spinner>' });
            getManufacturers();
        }

        function getManufacturers() {
            var manufacturers = Manufacturer.$search();
            manufacturers.$then(function(data) {
                vm.manufacturers = data.$response.data.manufacturers;
                $ionicLoading.hide();
            });
        }
    }
})();
