(function() {
	'use strict';

	angular
		.module('app.services')
		.factory('Category', Category);

	Category.$inject = ['restmod'];

	/* @ngInject */
	function Category(restmod) {
        return restmod.model('http://ittc-dev.astate.edu/abhishek/inventory-backend/public/api/member/categories').mix({
			products: { hasMany: 'Product' }
		});
	}
})();
