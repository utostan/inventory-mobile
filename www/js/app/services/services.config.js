(function () {
    'use strict';

    angular
        .module('app.services')
        .config(config);

    config.$inject = ['restmodProvider'];

    function config(restmodProvider) {
        restmodProvider.rebase('AMSApi');
    }
})();
