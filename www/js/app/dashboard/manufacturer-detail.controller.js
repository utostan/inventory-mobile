(function() {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('ManufacturerDetailController', ManufacturerDetailController);

    ManufacturerDetailController.$inject = ['Manufacturer', '$stateParams', '$ionicLoading'];

    /* @ngInject */
    function ManufacturerDetailController(Manufacturer, $stateParams, $ionicLoading) {
        var vm = this;
        vm.products = [];
        vm.getManufacturer = getManufacturer;
        vm.getProducts = getProducts;
        vm.imagePath = 'http://ittc-dev.astate.edu/abhishek/inventory-backend/public/images/';

        activate();

        function activate() {
            $ionicLoading.show({ template: '<ion-spinner icon="ripple"></ion-spinner>' });
            var manufacturer = getManufacturer();
            getProducts(manufacturer);
        }

        function getManufacturer()
        {
            var manufacturer = Manufacturer.$find($stateParams.manufacturerId);
            manufacturer.$then(function(data) {
                vm.manufacturer = data.$response.data.manufacturer;
            });

            return manufacturer;
        }

        function getProducts(manufacturer)
        {
            var products = manufacturer.products.$fetch();
            products.$then(function(data) {
                vm.products = products.$response.data.products;
                $ionicLoading.hide();
            });
        }
    }
})();
