(function () {
    'use strict';

    angular
        .module('app.dashboard')
        .config(config);

    config.$inject = ['$stateProvider'];

    function config($stateProvider) {
        $stateProvider
            .state('app.manufacturer', {
                url: '/manufacturer/:manufacturerId',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/manufacturer.html',
                        controller: 'ManufacturerDetailController',
                        controllerAs: 'manufacturer'
                    }
                }
            });
    }
})();
