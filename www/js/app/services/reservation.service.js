(function() {
	'use strict';

	angular
		.module('app.services')
		.factory('Reservation', Reservation);

	Reservation.$inject = ['restmod'];

	/* @ngInject */
	function Reservation(restmod) {
        return restmod.model('http://ittc-dev.astate.edu/abhishek/inventory-backend/public/api/member/reservations').mix({
			item: { belongsTo: 'Item' }
		});
	}
})();
