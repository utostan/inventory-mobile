(function () {
    'use strict';

    angular
        .module('app.dashboard')
        .config(config);

    config.$inject = ['$stateProvider'];

    function config($stateProvider) {
        $stateProvider
            .state('app.category', {
                url: '/category/:categoryId',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/category.html',
                        controller: 'CategoryDetailController',
                        controllerAs: 'category'
                    }
                }
            });
    }
})();
