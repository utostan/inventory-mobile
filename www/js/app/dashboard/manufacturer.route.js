(function () {
    'use strict';

    angular
        .module('app.dashboard')
        .config(config);

    config.$inject = ['$stateProvider'];

    function config($stateProvider) {
        $stateProvider
            .state('app.manufacturers', {
                url: '/manufacturers',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/manufacturers.html',
                        controller: 'ManufacturerController',
                        controllerAs: 'manufacturer'
                    }
                }
            });
    }
})();
