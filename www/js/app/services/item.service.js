(function() {
	'use strict';

	angular
		.module('app.services')
		.factory('Item', Item);

	Item.$inject = ['restmod'];

	/* @ngInject */
	function Item(restmod) {
        return restmod.model('http://ittc-dev.astate.edu/abhishek/inventory-backend/public/api/member/items').mix({
            product: { belongsTo: 'Product' }
        });
	}
})();
