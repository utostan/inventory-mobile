(function () {
    'use strict';

    angular
        .module('app.dashboard')
        .config(config);

    config.$inject = ['$stateProvider'];

    function config($stateProvider) {
        $stateProvider
            .state('app.welcome', {
                url: '/welcome',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/welcome.html',
                        controller: 'WelcomeController',
                        controllerAs: 'welcome'
                    }
                }
            });
    }
})();
