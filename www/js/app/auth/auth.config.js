(function () {
    'use strict';

    angular
        .module('app.auth')
        .config(config);

    config.$inject = ['$authProvider'];

    function config($authProvider) {
        $authProvider.loginUrl = 'http://ittc-dev.astate.edu/abhishek/inventory-backend/public/api/authenticate';
        $authProvider.signupUrl = 'http://ittc-dev.astate.edu/abhishek/inventory-backend/public/api/register';
    }
})();
