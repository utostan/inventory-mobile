(function() {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('WelcomeController', WelcomeController);

    WelcomeController.$inject = ['Reservation', '$ionicLoading'];

    /* @ngInject */
    function WelcomeController(Reservation, $ionicLoading) {
        var vm = this;
        vm.getReservations = getReservations;

        activate();

        function activate() {
            $ionicLoading.show({ template: '<ion-spinner icon="ripple"></ion-spinner>' });
            getReservations();
        }

        function getReservations() {
            var reservations = Reservation.$search();
            reservations.$then(function(data) {
                vm.reservations = data.$response.data.reservations;
                $ionicLoading.hide();
            });
        }
    }
})();
